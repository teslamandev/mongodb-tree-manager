import asyncio
import logging

from aiohttp import web
from aiohttp_apiset.middlewares import jsonify

from rest_api.settings import get_config
from rest_api.storage import NodeMongoDAO, UserMongoDAO
from rest_api.utils import init_mongo
from rest_api.views import routes

try:
    import uvloop
    uvloop.install()
except ImportError:
    logging.debug("Library uvloop is not available")


def setup_mongo(app, config):
    mongo = init_mongo(app, config['mongo'])

    async def close_mongo():
        mongo.client.close()

    app.on_shutdown.append(close_mongo)
    return mongo


def create_app():
    config = get_config()
    loop = asyncio.get_event_loop()
    app = web.Application(loop=loop, middlewares=[jsonify])

    mongo = setup_mongo(app, config)

    app['config'] = config
    app['users'] = UserMongoDAO(mongo)
    app['nodes'] = NodeMongoDAO(mongo)

    app.add_routes(routes)

    return app
