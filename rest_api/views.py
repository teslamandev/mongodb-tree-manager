import json
from datetime import datetime, timedelta

from aiohttp import web

from .storage import User, Node
from .security import (
    generate_password_hash,
    check_password_match,
    auth_required,
    generate_jwt,
)

routes = web.RouteTableDef()


@routes.view('/api/v1')
class Root(web.View):

    @auth_required
    async def get(self):
        """Api root"""
        payload = {
            'ok': True,
            'register': self.request.app.router['register'].url_for().path,
            'login': self.request.app.router['login'].url_for().path,
            'insert_node': self.request.app.router['insert_node'].url_for().path,
        }
        return web.json_response(payload, status=web.HTTPOk.status_code)


@routes.view('/api/v1/auth/login', name='login')
class Login(web.View):

    async def post(self):
        try:
            body = json.loads(await self.request.json())
        except json.JSONDecodeError:
            return web.json_response(
                {"ok": False, "error": "Invalid json format"},
                status=web.HTTPBadRequest.status_code,)

        db = self.request.app['users']
        username, password = body.get('username'), body.get('password')
        fail_data = {"ok": False, "error": "Invalid credentials"}

        if not username or not password:
            return web.json_response(fail_data, status=web.HTTPUnauthorized.status_code)

        try:
            user: User = await db.get_by_username(username)
            check_password_match(user.password_hash, password)
        except (User.DoesNotExist, User.PasswordDoesNotMatch):
            return web.json_response(fail_data, status=web.HTTPForbidden.status_code)

        jwt_config = self.request.app['config']['jwt']
        expire = datetime.utcnow() + timedelta(seconds=jwt_config['exp_delta_seconds'])
        payload = {
            'user_id': user.id,
            'username': user.username,
            'expire': datetime.isoformat(expire)
        }
        token = generate_jwt(payload, jwt_config)
        success_data = {"ok": True, "token": token}
        return web.json_response(success_data, status=web.HTTPOk.status_code)


@routes.view('/api/v1/nodes/insert', name='insert_node')
class InsertNode(web.View):

    @auth_required
    async def post(self):
        db = self.request.app['nodes']

        try:
            body = json.loads(await self.request.json())
        except (json.JSONDecodeError, TypeError):
            return web.json_response(
                {"ok": False, "error": "Invalid json"},
                status=web.HTTPBadRequest.status_code,)
        try:
            node = await db.create(Node.from_dict(body))
        except Exception as e:
            fail_data = {"ok": False, "error": f"Failed to insert node {e.args}"}
            return web.json_response(fail_data, status=web.HTTPConflict.status_code)

        success_data = {
            "ok": True,
            "node_id": node.id,
            "message": f"Inserted node {node.id} with parent {node.parent}",
        }
        return web.json_response(success_data, status=web.HTTPCreated.status_code)


@routes.view('/api/v1/nodes/search')
class FullTextSearch(web.View):

    @auth_required
    async def get(self):
        db = self.request.app['nodes']

        try:
            body = json.loads(await self.request.json())
        except (json.JSONDecodeError, TypeError):
            return web.json_response(
                {"ok": False, "error": "Invalid json"},
                status=web.HTTPBadRequest.status_code,)

        try:
            node = await db.get_by_text(body['text'])
        except Node.DoesNotExist:
            ancestors = []
        else:
            ancestors = node.ancestors

        return web.json_response(
            {"ok": True, "ancestors": ancestors},
            status=web.HTTPOk.status_code,
        )


@routes.view('/api/v1/nodes/{id}', name='node_detail')
class NodeDetail(web.View):

    @auth_required
    async def get(self):
        db = self.request.app['nodes']
        id = self.request.match_info['id']

        try:
            node = await db.get_by_id(id)
        except (ValueError, Node.DoesNotExist):
            ancestors = []
        else:
            ancestors = node.ancestors

        return web.json_response(
            {"ok": True, "ancestors": ancestors},
            status=web.HTTPOk.status_code,
        )


#### TEST ENDPOINTS HERE

@routes.view('/api/v1/auth/register', name='register')
class Register(web.View):
    """Register a user for test purposes."""

    async def post(self):
        db = self.request.app['users']

        try:
            body = json.loads(await self.request.json())
        except (json.JSONDecodeError, TypeError):
            return web.json_response(
                {"ok": False, "error": "Invalid json format"},
                status=web.HTTPBadRequest.status_code,)

        username, password = body.get('username'), body.get('password')
        fail_data = {"ok": False, "error": "Username or password is not provided"}

        if not username or not password:
            return web.json_response(fail_data, status=web.HTTPUnauthorized.status_code)

        password_hash = generate_password_hash(password)
        user = User(username=username, password_hash=password_hash)
        try:
            await db.create(user)
        except Exception:
            return web.json_response(
                {"ok": False, "error": f"Failed to create user '{username}'"},
                status=web.HTTPConflict.status_code,
            )

        success_data = {"ok": True, "message": f"User '{username}' created successfully"}
        return web.json_response(success_data, status=web.HTTPCreated.status_code)