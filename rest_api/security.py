import base64
import functools
from typing import Dict

import bcrypt
import jwt
from aiohttp import web

from .storage import User


def generate_password_hash(password, salt_rounds=16):
    password_bin = str(password).encode("utf-8")
    hashed = bcrypt.hashpw(password_bin, bcrypt.gensalt(salt_rounds))
    encoded = base64.b64encode(hashed)
    return encoded.decode("utf-8")


def generate_jwt(payload: Dict, jwt_config: Dict) -> str:
    return jwt.encode(payload, jwt_config['secret'], jwt_config['algorithm']).decode('utf-8')


def decode_jwt(token: str, jwt_config: Dict) -> Dict:
    return jwt.decode(token, jwt_config['secret'], jwt_config['algorithm'])


def check_password_match(encoded, password):
    password = str(password).encode("utf-8")
    encoded = encoded.encode("utf-8")

    hashed = base64.b64decode(encoded)
    is_correct = bcrypt.hashpw(password, hashed) == hashed

    if not is_correct:
        raise User.PasswordDoesNotMatch()


def auth_required(f):
    @functools.wraps(f)
    async def wrapped(self):
        token_header = self.request.headers.get("authorization", "")

        splitted = token_header.split()
        if not token_header or len(splitted) != 2 or splitted[0] != "Bearer":
            return web.json_response(
                {"ok": False, "error": "Authorization required"},
                status=web.HTTPUnauthorized.status_code,
            )

        token = splitted[1]
        jwt_config = self.request.app['config']['jwt']

        try:
            _payload = decode_jwt(token, jwt_config)

        except jwt.DecodeError:
            return web.json_response(
                {"ok": False, "error": "Invalid authorization token"},
                status=web.HTTPUnauthorized.status_code, )

        except jwt.ExpiredSignatureError:
            return web.json_response(
                {"ok": False, "error": "Authorization token expired"},
                status=web.HTTPUnauthorized.status_code, )

        return await f(self)

    return wrapped
