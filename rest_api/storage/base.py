import abc
from typing import Iterable, TypeVar

__all__ = ("AbstractDAO",)

Obj = TypeVar("Obj")


class AbstractDAO(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def create(self, obj: Obj) -> Obj:
        pass

    @abc.abstractmethod
    def update(self, obj: Obj) -> Obj:
        pass

    @abc.abstractmethod
    def get_all(self) -> Iterable[Obj]:
        pass

    @abc.abstractmethod
    def get_by_id(self, obj_id: str) -> Obj:
        pass
