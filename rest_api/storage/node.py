from typing import Iterable, Dict

import bson

from rest_api.storage.base import AbstractDAO

__all__ = ("Node", "NodeMongoDAO",)


class Node:

    def __init__(self, id: str = None, text: str = None, parent: str = None, ancestors: Iterable[str] = None):
        self.id = id
        self.text = text
        self.parent = parent
        self.ancestors = ancestors or []  # list of ancestors' ids

    @classmethod
    def to_dict(cls, node) -> Dict:
        return {
            "id": node.id,
            "text": node.text,
            "parent": node.parent,
            "ancestors": list(node.ancestors),
        }

    @classmethod
    def from_dict(cls, node_dict: Dict):
        return cls(
            id=node_dict.get("id"),
            text=node_dict["text"],
            parent=node_dict.get("parent"),
            ancestors=node_dict.get('ancestors'),
        )

    class DoesNotExist(Exception):
        pass


class NodeMongoDAO(AbstractDAO):
    """Implement 'Array of ancestors' mongodb tree pattern."""

    def __init__(self, mongo_database):
        self.mongo_database = mongo_database
        self.collection.create_index("text")

    @property
    def collection(self):
        return self.mongo_database["nodes"]

    @classmethod
    def to_bson(cls, node: Node):
        result = {
            "text": node.text,
            "parent": node.parent,
            "ancestors": [bson.ObjectId(ancestor_id) for ancestor_id in node.ancestors],
        }
        if node.id:
            result["_id"] = bson.ObjectId(node.id)
        return result

    @classmethod
    def from_bson(cls, document) -> Node:
        document["id"] = str(document.pop("_id"))
        document["ancestors"] = [str(ancestor_id) for ancestor_id in document['ancestors']]
        return Node(**document)

    async def create(self, node: Node) -> Node:
        try:
            parent_data = await self.get_by_id(node.parent)
        except Node.DoesNotExist:
            if node.parent:
                node.ancestors = [node.parent]
        else:
            node.ancestors = parent_data.ancestors
            node.ancestors.append(node.parent)

        node_data = await self.collection.insert_one(self.to_bson(node))
        node.id = str(node_data.inserted_id)
        return node

    async def update(self, node: Node) -> Node:
        node_id = bson.ObjectId(node.id)
        await self.collection.update_one({"_id": node_id}, {"$set": self.to_bson(node)})
        return node

    async def get_all(self) -> Iterable[Node]:
        async for document in self.collection.find():
            yield self.from_bson(document)

    async def get_by_id(self, node_id: str) -> Node:
        try:
            return await self._get_by_query({"_id": bson.ObjectId(node_id)})
        except bson.errors.InvalidId:
            raise ValueError

    async def get_by_text(self, text: str):
        return await self._get_by_query({"text": text})

    async def _get_by_query(self, query) -> Node:
        document = await self.collection.find_one(query)
        if document is None:
            raise Node.DoesNotExist()
        return self.from_bson(document)
