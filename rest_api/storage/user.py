from typing import Iterable

import bson

from rest_api.storage.base import AbstractDAO

__all__ = ("User", "UserMongoDAO",)


class User:

    def __init__(self, id: str = None, username: str = None, password_hash: str = None, superuser: bool = False, active: bool = True, permissions: Iterable = None):
        self.id = id
        self.username = username
        self.password_hash = password_hash
        self.superuser = superuser
        self.active = active

    class DoesNotExist(Exception):
        pass

    class PasswordDoesNotMatch(Exception):
        pass


class UserMongoDAO(AbstractDAO):

    def __init__(self, mongo_database):
        self.mongo_database = mongo_database
        self.collection.create_index("username", unique=True)

    @property
    def collection(self):
        return self.mongo_database["users"]

    @classmethod
    def to_bson(cls, user: User):
        result = {
            k: v
            for k, v in user.__dict__.items()
            if v is not None
        }
        if "id" in result:
            result["_id"] = bson.ObjectId(result.pop("id"))
        return result

    @classmethod
    def from_bson(cls, document) -> User:
        document["id"] = str(document.pop("_id"))
        return User(**document)

    async def create(self, user: User) -> User:
        user_data = await self.collection.insert_one(self.to_bson(user))
        user.id = str(user_data.inserted_id)
        return user

    async def update(self, user: User) -> User:
        user_id = bson.ObjectId(user.id)
        await self.collection.update_one({"_id": user_id}, {"$set": self.to_bson(user)})
        return user

    async def get_all(self) -> Iterable[User]:
        async for document in self.collection.find():
            yield self.from_bson(document)

    async def get_by_id(self, user_id: str) -> User:
        try:
            return await self._get_by_query({"_id": bson.ObjectId(user_id)})
        except bson.errors.InvalidId:
            raise ValueError

    async def get_by_username(self, username: str):
        return await self._get_by_query({"username": username})

    async def _get_by_query(self, query) -> User:
        document = await self.collection.find_one(query)
        if document is None:
            raise User.DoesNotExist()
        return self.from_bson(document)
