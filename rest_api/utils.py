import motor.motor_asyncio as aiomotor


def init_mongo(app, config):
    host, port = config['host'], config['port']
    mongo_uri = f"mongodb://{host}:{port}"
    conn = aiomotor.AsyncIOMotorClient(
        mongo_uri,
        io_loop=app.loop,
        maxPoolSize=config['max_pool_size'])
    db_name = config['database']
    return conn[db_name]
