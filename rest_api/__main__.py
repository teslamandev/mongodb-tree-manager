"""
Entry point of the app. Run it with 'python -m rest_api'.
"""
from aiohttp import web

from .main import create_app

app = create_app()
host, port = app['config']['host'], app['config']['port']

web.run_app(app, host=host, port=port)
