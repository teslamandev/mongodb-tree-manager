import argparse
import pathlib
import trafaret as T
from trafaret_config import commandline

__all__ = ('get_config',)

BASE_DIR = pathlib.Path(__file__).parent.parent
DEFAULT_CONFIG_PATH = BASE_DIR / 'config' / 'config.yaml'

TRAFARET = T.Dict({
    T.Key('mongo'):
        T.Dict({
            'database': T.String(),
            'host': T.String(),
            'port': T.Int(),
            'max_pool_size': T.Int(),
        }),
    T.Key('jwt'):
        T.Dict({
            'secret': T.String(),
            'algorithm': T.String(),
            'exp_delta_seconds': T.Int(),
        }),
    T.Key('host'): T.IP,
    T.Key('port'): T.Int(),
})


def get_config():
    parser = argparse.ArgumentParser()
    commandline.standard_argparse_options(parser, default_config=DEFAULT_CONFIG_PATH)
    options, unknown = parser.parse_known_args()
    config = commandline.config_from_options(options, TRAFARET)
    return config
