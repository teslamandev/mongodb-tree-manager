.PHONY: run
run:
	@docker-compose up -d

.PHONY: build
build:
	@docker-compose build

.PHONY: clean
clean:
	@rm -rf `find . -name __pycache__`

.PHONY: test
test: clean
	pytest --cov=app

.PHONY: testcov
testcov:
	pytest --cov=app && (echo "building coverage html, view at './htmlcov/index.html'"; coverage html)
